---
layout: markdown_page
title: "Plato Career Matrix Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes
 
| Property        | Value           |
|-----------------|-----------------|
| Date Created    | March 6, 2020 |
| Target End Date | April 30, 2020 |
| Slack           | [#platohq-coaching=program]() (only accessible from within the company) |
| Google Doc      | [Career Matrix Working Group Agenda](https://docs.google.com/document/d/1gTAXEdNXNiFGcXuKS5seGrz2GLbeftv56fT9NvIEZmg/edit?usp=sharing) (only accessible from within the company) |
| Issue Board     | [Issue board](https://gitlab.com/groups/gitlab-com/-/boards/1360344?&label_name[]=wg-internship-pilot)|

## Business Goal

Deliver 4 career matrices in Engineering as part of PlatoHQ program in response to CultureAmp feedback aligned with this [FY21-Q1 Engineering KR](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6374)

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Stakeholder | Eric Johnson          | VP of Engineering              |
| Facilitator           | Roos Takken           | People Business Partner, Engineering |
| Function Lead         | Jean du Plessis       | Engineering Manager, Static Site Editor |
| Function Lead         | Darva Satcher         | Engineering Manager for Create: Knowledge and Create: Editor |
| Function Lead         | Shaun McCann          | Support Engineering Manager - APAC |
| Function Lead         | Tanya Pazitny         | Quality Engineering Manager, Secure & Enablement |
| Function Lead         | Marin Jankovski       | Senior Engineering Manager, Delivery and Scalability |

## Exit Criteria

To be determined.
