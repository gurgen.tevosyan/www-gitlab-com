---
layout: handbook-page-toc
title: "TAM and Support Interaction"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Customer Support

GitLab offers [a variety of support options](/support/) for all customers and users on both paid and free tiers. For customers on Standard and Priority support tiers please address the below items when [submitting a support ticket](https://support.gitlab.com/hc/en-us):

1. Provide as much detail as possible during the first submission of the ticket

2. Summary of issue (when did it start, how frequently, impact on organization, etc.)
    * Detailed steps to recreate
    * Current behavior
    * Expected behavior
    * Any recent changes to Gitlab, its components, dependencies, or the services it's hosted on?
    * Attach logs and screenshots (avoid attaching .doc or .pdf files)

3. Try and avoid requesting a call during the initial ticket submission. We would like to keep all communication within the ticket and attempt to resolve the issue here before going to a call.

4. If a call is necessary, the support engineer will invite your team to a call via the ticket

5. If a support engineer requests follow up items, please make sure to respond back with these items. This will help us resolve the issue as quickly as possible

### Zendesk Tickets

When a TAM is assigned to an account in Salesforce, any time that a user from that account submits a support ticket, the TAM will receive an email to let them know, which includes a link to the Zendesk ticket and an [automatically-created](https://about.gitlab.com/handbook/support/support-ops/#salesforce---zendesk-sync) Salesforce case. The email will also include the account name, contact name and email, and the date the ticket was opened. TAMs should receive the email within 1-2 hours of the ticket being opened.

In the email notifying TAMs of new tickets, the included Zendesk ticket link currently is a .json file which is difficult to read and sometimes doesn't load. There are a few ways to work around this:

1. Open the Salesforce case link instead. You will be unable to take any action on the ticket, but you will be able to read it.
1. Open the Zendesk ticket link, then edit the URL so it takes you to the ticket itself rather than the .json. For example, https://gitlab.zendesk.com/api/v2/tickets/123456.json is the format of the ticket link in the email, but by deleting `/api/v2` and `.json` and then going to that new link, https://gitlab.zendesk.com/tickets/123456, you will be taken directly to the ticket.
1. Go to [Zendesk](https://gitlab.zendesk.com/agent/) and search for the ticket (by copying the ticket number or searching for the customer).
1. Get the [Redirector chrome extension](https://chrome.google.com/webstore/detail/redirector/ocgpenflpmgnfapjedencafcfakcekcd?hl=en) and import the below configuration:

```
{
    "createdBy": "Redirector v3.5.2",
    "createdAt": "2020-03-11T16:25:30.936Z",
    "redirects": [
        {
            "description": "Redirect zendesk tickets",
            "exampleUrl": "https://gitlab.zendesk.com/api/v2/tickets/12345.json",
            "exampleResult": "https://gitlab.zendesk.com/tickets/12345",
            "error": null,
            "includePattern": "https://gitlab.zendesk.com/api/v2/tickets/([0-9]+).json",
            "excludePattern": "",
            "patternDesc": "",
            "redirectUrl": "https://gitlab.zendesk.com/tickets/$1",
            "patternType": "R",
            "processMatches": "noProcessing",
            "disabled": false,
            "appliesTo": [
                "main_frame"
            ]
        }
    ]
}
```

### Helpful links:
- [Official GitLab Support Documentation](https://about.gitlab.com/support/)
- [Support Handbook (SLA + Tiers)](https://about.gitlab.com/handbook/support/)
- [GitLab.com Status and Notifications](https://status.gitlab.com/)
- [Customer Onboarding](/handbook/customer-success/tam/onboarding)
- [Escalation Process](/handbook/customer-success/tam/escalations)
