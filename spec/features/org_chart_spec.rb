require 'spec_helper'

describe 'org-chart', :js do
  before do
    visit '/company/team/org-chart'
  end

  context 'collapse/expand all button' do
    it 'collapses and expands the nodes' do
      button_class = '.js-toggle-expand-collapse'
      collapse_expand_button = page.find(button_class)

      # Checks the default state
      expect(collapse_expand_button).to have_text('Collapse All')
      expect(all('.node.has-tree.is-expanded').length).to be > 0

      # Check the collapsing behavior
      collapse_expand_button.click

      page.find(button_class, text: 'Expand All')
      expect(all('.node.has-tree.is-expanded').length).to eq(0)

      # Check the expanding behavior
      collapse_expand_button.click

      page.find(button_class, text: 'Collapse All')
      expect(all('.node.has-tree.is-expanded').length).to be > 0
    end
  end
end
